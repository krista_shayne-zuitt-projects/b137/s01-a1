package ActivityZeroOne;

public class DataTypesAndVariables {
    public static void main(String[] args) {
        String firstName = "Krista ";
        String lastName = "Miniano";
        String concatenateName = firstName.concat(lastName);
        float english = 85;
        float mathematic = 90;
        float science = 80;

        System.out.println("Name: " +concatenateName);
        System.out.println("Average of three subject is: " +
                (english + mathematic + science ) / 3);

    }
}
